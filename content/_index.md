---
title: Chokola
---

# What is it ?

Chokola is a simple tool to convert a YAML into a HTML table.

# How to install it ?

Chokola is coded in python, so it is available on pip.

```
$ pip3 install chokola
```

# How to use it ?

Simply give a YAML file to Chokola and wait for the HTML Table.

```
# table.yaml
"column 1":
  - value 1
  - value 3
  - value 5
"column 2":
  - value 2
  - value 4
  - value 6
```

```
$ chokola table.yaml
<table>
  <tr>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-leef ">
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef ">
    <td>value 3</td>
    <td>value 4</td>
  </tr>
  <tr class="chokola-leef ">
    <td>value 5</td>
    <td>value 6</td>
  </tr>
</table>
```

Chokola print the result directly to `stdout`.
So, we can redirect the ouput into a file if we want to store the result.

```
$ chokola table.yaml > table.html
```

# Type of tables supported

## Simple list

Just a list, simplier table doesn't exist.

```
# table.yaml
- value 1
- value 2
- value 3
```

```
$ chokola table.yaml
<table>
  <tr>
    <td>value 1</td>
  </tr>
  <tr>
    <td>value 2</td>
  </tr>
  <tr>
    <td>value 3</td>
  </tr>
</table>
```

Résultat:

<table>
  <tr>
    <td>value 1</td>
  </tr>
  <tr>
    <td>value 2</td>
  </tr>
  <tr>
    <td>value 3</td>
  </tr>
</table>

## Table with columns

To add colum to your table, just use the following format.

```
# table.yaml
"column 1":
  - value 1
  - value 3
"column 2":
  - value 2
  - value 4
```

```
$ chokola table.yaml
<table>
  <tr>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-leef ">
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef ">
    <td>value 3</td>
    <td>value 4</td>
  </tr>
</table>
```

Résultat:

<table>
  <tr>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-leef ">
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef ">
    <td>value 3</td>
    <td>value 4</td>
  </tr>
</table>

## Table with columns and lines

To add name to your table lines, just use the following format.

```
# table.yaml
"line 1":
  "column 1": value 1
  "column 2": value 2
"line 2":
  "column 1": value 3
  "column 2": value 4
```

```
$ chokola table.yaml
<table>
  <tr>
    <th> </th>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-leef chokola-line-1 ">
    <td>line 1</td>
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef chokola-line-2 ">
    <td>line 2</td>
    <td>value 3</td>
    <td>value 4</td>
  </tr>
</table>

```

Résultat:

<table>
  <tr>
    <th> </th>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-leef chokola-line-1 ">
    <td>line 1</td>
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef chokola-line-2 ">
    <td>line 2</td>
    <td>value 3</td>
    <td>value 4</td>
  </tr>
</table>

## Table with colums, lines and titles

```
# table.yaml
"title 1":
  "line 1":
    "column 1": value 1
    "column 2": value 2
  "line 2":
    "column 1": value 3
    "column 2": value 4
```

```
$ chokola table.yaml
<table>
  <tr>
    <th> </th>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-level-0 chokola-title-1 ">
    <td>title 1</td>
    <td> </td>
    <td> </td>
  </tr>
  <tr class="chokola-leef chokola-line-1 ">
    <td>line 1</td>
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef chokola-line-2 ">
    <td>line 2</td>
    <td>value 3</td>
    <td>value 4</td>
  </tr>
</table>
```

<table>
  <tr>
    <th> </th>
    <th>column 1</th>
    <th>column 2</th>
  </tr>
  <tr class="chokola-level-0 chokola-title-1 ">
    <td>title 1</td>
    <td> </td>
    <td> </td>
  </tr>
  <tr class="chokola-leef chokola-line-1 ">
    <td>line 1</td>
    <td>value 1</td>
    <td>value 2</td>
  </tr>
  <tr class="chokola-leef chokola-line-2 ">
    <td>line 2</td>
    <td>value 3</td>
    <td>value 4</td>
  </tr>
</table>

# HTML classes

Chokola add automatically certain HTML classes to the element `tr`.

chokola-level-NUMBER
: NUMBER will be the title level

chokola-leef
: This line is a leef of the table

chokola-NAME
: NAME will be the name of the line

You can add your own classes to the table with the following options `--table-classes`, `--tr-classes`, `--th-classes` and `--td-classes`.

# Is there more to know ?

Print version.

```
$ chokola -v
```

Print the help.

```
$ chokola -h
```

# Hope you will enjoy it ...
